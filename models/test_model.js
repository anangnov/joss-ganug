"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "users",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: true
      },
      card: {
        type: DataTypes.STRING,
        allowNull: true
      },
      count_wa_send: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      last_checkIn: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      // manifest_time: {
      //   type: DataTypes.TIME,
      //   allowNull: true
      // },
      // city_name: {
      //   type: DataTypes.STRING,
      //   allowNull: true
      // },
      // created_at: {
      //   type: DataTypes.DATE,
      //   allowNull: false,
      //   defaultValue: DataTypes.NOW
      // },
      // updated_at: {
      //   type: DataTypes.DATE,
      //   allowNull: false,
      //   defaultValue: DataTypes.NOW
      // }
    },
    {
      tableName: "users",
      timestamps: false
    }
  );
};
