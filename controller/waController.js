"use-strict";

const unirest = require("unirest");
const async = require("async");
const models = require("../models");

const list = (req, res) => {
  let urlText = "/wareguler/api/sendWA";
  let urlMedia = "/wareguler/api/sendWAFile";
  let urlVoice = "/voice/api/sendvoice";
  let master = {
    userKey: "50224c5ede95",
    passKey: "7ae515950ddaa398257bf10d",
    adminNumber: "6289667668888",
    caption: "Thank You for Attending",
    bannerImageUrl: "http://wedding.nugroho.xyz/images/banner/bn2.png", //"https://bit.ly/3bXr65w",
    greetingText:
      "\nKami Segenap Keluarga Besar \n\n*Dian Nugroho, S.Kom* dan *Dini Khusnul Siti Khotimah, S.Pd* \n\nMengucapkan Terima Kasih \nAtas Kehadiran serta Doa Restunya \nPada Acara Pernikahan Kami \n\n_Mojokerto, Senin 23 November 2020_ \n\n\n✅",
    dateTime: new Date()
  };
  let dataUser = {};

  async.waterfall(
    [
      function validation(callback) {
        callback(null, {});
      },
      function getUserData(data, callback) {
        models.sequelize
          .query("SELECT * FROM users where card = '" + req.query.card + "'")
          .then((raw, err) => {
            if (err) return req.output(req, res, err, "error", 400);
            if (raw[0].length) {
              dataUser.name = raw[0][0].name;
              dataUser.phone = raw[0][0].phone;
            }
            callback(null, data);
          });
      },
      function register(data, callback) {
        if (dataUser.name) {
          if (dataUser.phone) {
            const sql =
              "UPDATE users SET count_wa_send=0 where card='" +
              req.query.card +
              "'";
            models.sequelize.query(sql).then(() => {});
          } else {
            // abaikan
          }
        } else {
          const today = Date("dddd, MMMM Do YYYY H:i:s").toLocaleString(
            "en-US",
            { timeZone: "Asia/Jakarta" }
          );
          const username = req.query.name ? req.query.name : "VIP Guest";
          unirest
            .post(`${req.API_URL}` + urlText)
            .headers(req.API_HEADER)
            .send({
              userkey: master.userKey,
              passkey: master.passKey,
              to: master.adminNumber,
              message:
                "New Smartcard unregister detected. \n" +
                "Smartcard : '" +
                req.query.card +
                "' \n" +
                "Datetime : " +
                today // enter = %0a
            })
            .then(response => {
              console.log("==================");
              console.log("New card " + req.query.card + " detected");
              console.log("Datetime : " + today);
              console.log(response.status);
              console.log("==================");
            });

          unirest
            .post(`${req.API_URL}` + urlText)
            .headers(req.API_HEADER)
            .send({
              userkey: master.userKey,
              passkey: master.passKey,
              to: master.adminNumber,
              message: req.query.card // enter = %0a
            })
            .then(() => {});

          const sql =
            "INSERT INTO users (name,phone,card,count_wa_send) VALUES ('" +
            username +
            "'," +
            (req.query.phone ? "'" + req.query.phone + "'" : null) +
            "," +
            (req.query.card ? "'" + req.query.card + "'" : null) +
            ",'1')";
          models.sequelize.query(sql).then(() => {});
        }

        callback(null, data);
      },
      function one(data, callback) {
        if (dataUser.phone)
          unirest
            .post(`${req.API_URL}` + urlMedia)
            .headers(req.API_HEADER)
            .send({
              userkey: master.userKey,
              passkey: master.passKey,
              to: req.query.to ? req.query.to : dataUser.phone,
              name: dataUser.name ? dataUser.name : "VIP Guest",
              // 'custom_uid' => isset($_GET['custom_uid']) ? $_GET['custom_uid'] : null,
              link: master.bannerImageUrl,
              caption: master.greetingText + master.dateTime
            })
            .then(response => {
              console.log("==================");
              console.log("caption " + master.greetingText + master.dateTime);
              console.log("send Media to " + dataUser.phone);
              console.log(response.status);
              console.log(response.body);
              console.log("==================");
            });

        callback(null, data);
      },
      function two(data, callback) {
        if (dataUser.phone)
          unirest
            .post(`${req.API_URL}` + urlVoice)
            .headers(req.API_HEADER)
            .send({
              userkey: master.userKey,
              passkey: master.passKey,
              to: req.query.to ? req.query.to : dataUser.phone,
              message:
                "kami segenap keluarga besar dian nugroho dan dini khusunul siti khotimah mengucapkan terima kasih atas kehadiran serta doa restunya. kami segenap keluarga besar dian nugroho dan dini khusunul siti khotimah mengucapkan terima kasih atas kehadiran serta doa restunya. sekian dan terima kasih"
            })
            .then(response => {
              console.log("==================");
              console.log("send Voice to " + dataUser.phone);
              console.log(response.status);
              console.log(response.body);
              console.log("==================");
            });
        callback(null, data);
      },
      function final(data, callback) {
        if (dataUser.name)
          data = {
            error: false,
            message: "Success"
          };

        callback(null, data);
      }
    ],
    (err, result) => {
      if (err || result.error != false) {
        console.log("invalid card : '", req.query.card + "'");
        return req.output(req, res, err, "error", 400);
      }
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { list };
