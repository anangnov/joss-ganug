"use-strict";

const unirest = require("unirest");
const async = require("async");

const list = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        callback(null, {});
      },
      function one(data, callback) {
        unirest
        .get(`${req.API_URL}/sendwa.php?card=02:B1:6F:71:90:2C:40`)
        .headers(req.API_HEADER)
        .send({})
        .then(response => {
          console.log('==================')
          console.log(response.status)
          console.log('run function one');
          console.log('==================')
        });

        callback(null, data);
      },
      function two(data, callback) {
        unirest
        .get(`${req.API_URL}/sendwa.php?card=12:12:12:12`)
        .headers(req.API_HEADER)
        .send({})
        .then(response => {
          console.log('==================')
          console.log(response.status)
          console.log('run function two');
          console.log('==================')
        });

        callback(null, data);
      },
      function final(data, callback) {
        data = {
          error: false,
          message: "Success"
        };

        callback(null, data);
      }
    ],
    (err, result) => {
      if (err) {
        return req.output(req, res, err, "error", 400);
      }

      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { list };
