const express = require("express");
const router = express.Router();
const testController = require("../controller/testController");
const waController = require("../controller/waController");
const notificationController = require("../controller/notificationController");

module.exports = app => {
  router.get("/send/wa", waController.list);
  router.get("/send/custom/notification", notificationController.list);

  app.use("/api", router);
};
